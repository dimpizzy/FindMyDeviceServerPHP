<?php
/*
"default": {
	"permissions": [
		"SEND_SMS",
		"READ_SMS"
	]
},
*/

$commands = file_get_contents(__DIR__."/main/commands.json");
$commands = json_decode($commands, true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>FindMyDevice - SMS commands</title>
	<link rel="stylesheet" href="./main/CSS/style.css">
</head>
<body>
	<header>FindMyDevice - SMS commands</header>
	<nav>
		<?php
		foreach($commands as $link => $_) {
			echo "<a href=\"#$link\">FMD $link</a>\n\t\t";
		}
		?>
	</nav>
	<main>
		<?php
		foreach($commands as $cmd => $info) {
		?>
		<section id="<?php echo $cmd; ?>">
			<h1><?php echo $info["title"]; ?></h1>
			<div>
				<h3>Description</h3>
				<?php
				if(!is_array($info["description"]))
					$info["description"] = [$info["description"]];

				echo implode("<br>\n", $info["description"]);
				?>
			</div>
			<div>
				<h3>Command</h3>
				<code><?php echo htmlentities($info["command"]); ?></code>
			</div>
			<div>
				<h3>Permissions</h3>
				<?php
				if(!is_array($info["permissions"]))
					$info["permissions"] = [$info["permissions"]];

				echo implode("<br>\n", $info["permissions"]);
				?>
			</div>
		</section>
		<?php
		}
		?>
	</main>
</body>
</html>