<?php
$_PUT = "";
if(($h = fopen("php://input", "r")) !== false) {
	while($line = fread($h, 1024)) {
		$_PUT .= $line;
	}
	fclose($h);
}
$_PUT = json_decode($_PUT, true) ?: [];

switch($_SERVER["REQUEST_METHOD"]) {
	case "GET":
		$_GET = $_PUT;
		$_PUT = [];
		break;
	case "POST":
		$_POST = $_PUT;
		$_PUT = [];
		break;
}

include_once(__DIR__."/conf.php");

$files = glob(__DIR__."/PHP/*.php");
foreach($files as $file) {
	include_once($file);
}
unset($files);


function exists($vars, $keys = [], $allowNull = false) {
	if(!is_array($vars)) return isset($vars) && !empty($vars);
	if(count($vars) < 1) return false;

	if(!is_array($keys)) $keys = [$keys];
	if(count($keys) < 1) return isset($vars) && !empty($vars);

	foreach($keys as $key) {
		if(empty($key) || !array_key_exists($key, $vars)) return false;
		if(!(isset($vars[$key]) && !empty($vars[$key])) && (!$allowNull && ($vars[$key] == NULL || $vars[$key] == 0))) return false;
	}
	return true;
}

function sqlquery(string $query, $params = []) {
	global $PDO;
	if(($stmt = $PDO->prepare($query)) == false) {
		return false;
	}

	foreach($params as $key=>&$param) {
		$stmt->bindParam($key, $param);
	}

	if($stmt->execute()) {
		$result = [];
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$result[] = $row;
		}
	} else {
		response($stmt->errorInfo());
	}

	return $result;
}

function parsePath($path, $basepath = __DIR__) {
	$path = realpath("$basepath/$path");
	if($path !== false && substr($path, 0, strlen($basepath)) == $basepath)
		return $path;
	return false;
}

function response($data, $ex = true) {
	if(is_array($data)) $data = json_encode($data);
	if($ex) exit($data);
	echo $data;
}
?>
