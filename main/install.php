<?php
error_reporting(E_ALL);
ini_set("display_errors", "1");

include_once(__DIR__."/conf.php");
include_once(__DIR__."/main.php");

$queries = [
"CREATE TABLE IF NOT EXISTS user(
	id int(15) NOT NULL AUTO_INCREMENT,
	username text NOT NULL,
	password varchar(64) NOT NULL,
	cmdLink varchar(255) DEFAULT NULL,
	attempt int(3) NOT NULL DEFAULT 0,
	attemptExp datetime DEFAULT NULL,
	pub text NOT NULL,
	pri text NOT NULL,
	login timestamp NOT NULL DEFAULT current_timestamp(),
	register timestamp NOT NULL DEFAULT current_timestamp(),
	PRIMARY KEY(id)
) DEFAULT CHARSET = utf8mb4;",

"CREATE TABLE IF NOT EXISTS token(
	id int(15) NOT NULL AUTO_INCREMENT,
	username text NOT NULL,
	token varchar(15) NOT NULL,
	tokenExp datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
) DEFAULT CHARSET = utf8mb4;",

"CREATE TABLE IF NOT EXISTS location(
	id int(15) NOT NULL AUTO_INCREMENT,
	username text NOT NULL,
	provider text NOT NULL,
	lon text NOT NULL,
	lat text NOT NULL,
	bat text NOT NULL,
	date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
) DEFAULT CHARSET = utf8mb4;",

"CREATE TABLE IF NOT EXISTS command(
	id int(15) NOT NULL AUTO_INCREMENT,
	username text NOT NULL,
	cmd varchar(15) NOT NULL,
	seen tinyint(1) NOT NULL DEFAULT 0,
	date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
) DEFAULT CHARSET = utf8mb4;",

"CREATE TABLE IF NOT EXISTS picture(
	id int(15) NOT NULL AUTO_INCREMENT,
	username text NOT NULL,
	pic mediumtext NOT NULL,
	date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
) DEFAULT CHARSET = utf8mb4;",

"CREATE EVENT IF NOT EXISTS deltoken
ON SCHEDULE EVERY 10 MINUTE
DO
	DELETE FROM token WHERE CURRENT_TIMESTAMP > ADDTIME(tokenExp, \"00:30\");
"
];

foreach($queries as $query) {
	sqlquery($query);
}

unlink(__FILE__);
?>
