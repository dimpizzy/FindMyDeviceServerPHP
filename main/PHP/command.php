<?php
function newCommand($username, $cmd) {
	sqlquery("INSERT INTO command(username, cmd) VALUES (:user, :cmd);", [
		":user" => $username,
		":cmd" => $cmd
	]);
	$sql = sqlquery("SELECT cmdLink FROM user WHERE username = :user;", [
		":user" => $username
	])[0];

	$url = str_replace("/UP?", "/message?", $sql["cmdLink"]);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		"Content-Type: application/json; charset=utf-8"
	]);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
		"message" => "magic may begin",
		"prioritiy" => 5
	], true));

	$res = curl_exec($ch);
	curl_close($ch);
	return $res;
}

function getCommand($username) {
	$sql = sqlquery("SELECT id, cmd FROM command WHERE seen = false AND username = :user;", [
		":user" => $username
	])[0];
	if(exists($sql)) {
		sqlquery("UPDATE command SET seen = true WHERE id = :id;", [
			":id" => $sql["id"]
		]);
		return $sql["cmd"];
	}
	return "";
}
?>