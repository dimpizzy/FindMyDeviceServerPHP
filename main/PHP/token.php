<?php
function genToken($len = false) {
	global $TLENGTH;
	if($len == false) $len = $TLENGTH;

	$token = openssl_random_pseudo_bytes($TLENGTH);
	$token = bin2hex($token);
	$token = str_split($token, 2);

	foreach($token as $i=>$t) {
		$t = hexdec($t);
		$t %= 69;
		$t += 48;
		if($t > 90 && $t < 135) $t += 6;
		if($t > 57 && $t < 66) $t += 7;
		$token[$i] = chr($t);
	}

	$token = implode("", $token);
	return $token;
}
?>