<?php
function checkToken($token) {
	return checkAccessToken($token);
}

function checkID($id, $hash) {
	checkValidID($id);
	checkLock($id);
	checkPass($id, $hash);
}



function checkAccessToken($token) {
	$sql = sqlquery("SELECT username, token, tokenExp FROM token WHERE token = :t;", [
		":t" => $token,
	])[0];

	if(exists($sql, ["username", "token", "tokenExp"], true)) {
		$exp = new DateTime($sql["tokenExp"]);
		$exp->add(new DateInterval("P15M"));
		if($exp > new DateTime("now")) {
			return $sql["username"];
		}
		sqlquery("DELETE FROM token WHERE token = :t;", [
			":t" => $sql["token"],
		])[0];
		response([
			"state" => "error",
			"message" => "AccessToken is not valid anymore"
		]);
	}
	response([
		"state" => "error",
		"message" => "AccessToken is not valid"
	]);
}



function checkValidID($id) {
	if(preg_match("/^[a-zA-Z0-9]*$/", $id) < 0) {
		response([
			"state" => "error",
			"message" => "not a valid ID"
		]);
	}
}

function checkLock($id) {
	$sql = sqlquery("SELECT attempt, attemptExp FROM user WHERE username = :user;", [
		":user" => $id
	])[0];
	if(exists($sql, ["attempt", "attemptExp"], true)) {
		$exp = new DateTime($sql["attemptExp"]);
		$exp->add(new DateInterval("P10M"));

		if($sql["attempt"] == 0 || ($sql["attempt"] < 3 && $exp > new DateTime("now"))) {
			return;
		}
	}
	newCommand($id, 423);
	response([
		"state" => "error",
		"message" => "ID is locked"
	]);
}

function checkPass($id, $hash) {
	$sql = sqlquery("SELECT attempt, password FROM user WHERE username = :user;", [
		":user" => $id
	])[0];
	if(exists($sql, ["attempt", "password"], true)) {
		if(hash_equals($sql["password"], $hash)) {
			if($sql["attempt"] > 0) {
				sqlquery("UPDATE user SET attempt = 0, attemptExp = NULL WHERE username = :user;", [
					":user" => $id
				]);
			}
			return;
		}
		sqlquery("UPDATE user SET attempt = attempt + 1, attemptExp = CURRENT_TIMESTAMP() WHERE username = :user;", [
			":user" => $id
		]);
	}
	response([
		"state" => "error",
		"message" => "User or Password is wrong"
	]);
}