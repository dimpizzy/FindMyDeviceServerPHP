<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["provider", "date", "bat", "lon", "lat", "IDT"])) {
	$username = checkToken($_POST["IDT"]);

	$date = new DateTime();
	$date->createFromFormat("U.v", strval($_POST["date"] / 1000));
	$date = $date->format("Y-m-d H:i:s.v");

	sqlquery("INSERT INTO location(username, provider, lon, lat, bat, date) VALUES (:user, :prov, :lon, :lat, :bat, :date);", [
		":user" => $username,
		":prov" => $_POST["provider"],
		":lon" => $_POST["lon"],
		":lat" => $_POST["lat"],
		":bat" => $_POST["bat"],
		":date" => $date
	]);

	exit;
	response([
		"state" => "ok"
	]);
}

if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	if(is_numeric($_PUT["Data"])) {
		$amount = intval($_PUT["Data"]);
		if(!is_int($amount) || $amount < 1) response([
			"state" => "error",
			"message" => "Amount is not a positiv integer"
		]);

		$sql = sqlquery(implode(" ", [
			"SELECT provider, lon, lat, bat, DATE_FORMAT(date, \"%Y-%m-%dT%H:%i:%s\") AS date FROM location",
			"WHERE username = :user",
			"ORDER BY date DESC",
			"LIMIT :amount;"
		]), [
			":user" => $username,
			":amount" => $amount
		]);

		response([
			"state" => "ok",
			"location" => $sql
		]);
	} else {
		$dates = $_PUT["Data"];
		if(!exists($date, ["start", "end"], true)) response([
			"state" => "error",
			"message" => "start or end date are missing"
		]);

		$date["start"] = DateTime::createFromFormat("Y-m-d\TH:i:s", $date["start"]);
		$date["end"] = DateTime::createFromFormat("Y-m-d\TH:i:s", $date["end"]);
		var_dump($date);exit;

		$sql = sqlquery(implode(" ", [
			"SELECT provider, lon, lat, bat, DATE_FORMAT(date, \"%Y-%m-%dT%H:%i:%s\") AS date FROM location",
			"WHERE username = :user AND date > :start AND date < :end",
			"ORDER BY date DESC;"
		]), [
			":user" => $username,
			":start" => $start->format("Y-m-d H:i:s"),
			":end" => $end->format("Y-m-d H:i:s")
		]);

		response([
			"state" => "ok",
			"location" => $sql
		]);
	}
}
?>