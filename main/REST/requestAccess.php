<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "Data"])) {
	checkID($_PUT["IDT"], $_PUT["Data"]);

	$aToken = genToken();
	sqlquery("INSERT INTO token(username, token) VALUES (:user, :t);", [
		":user" => $_PUT["IDT"],
		":t" => $aToken
	]);
	response([
		"IDT" => $_PUT["IDT"],
		"Data" => $aToken
	]);
}
?>