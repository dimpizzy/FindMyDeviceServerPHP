<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, "IDT")) {
	$username = checkToken($_PUT["IDT"]);

	$salt = "cafe";
	$sql = sqlquery("SELECT salt FROM user WHERE username = :user;", [
		":user" => $username
	])[0];
	if(exists($sql))
		$salt = $sql["salt"];

	response([
		"IDT" => $_PUT["IDT"],
		"Data" => $salt
	]);
}
?>