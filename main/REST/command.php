<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["IDT", "Data"])) {
	$username = checkToken($_POST["IDT"]);
	
	$res = newCommand($username, $_POST["Data"]);

	response([
		"state" => "ok",
		"Data" => $res
	]);
}

if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	$cmd = getCommand($username);

	response([
		"state" => "ok",
		"IDT" => $_PUT["IDT"],
		"Data" => $cmd
	]);
}
?>