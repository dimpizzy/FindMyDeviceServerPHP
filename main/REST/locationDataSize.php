<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "Data"])) {
	$username = checkToken($_PUT["IDT"]);

	$sql = sqlquery("SELECT id FROM location WHERE username = :user;", [
		":user" => $username
	]);
	response([
		"state" => "ok",
		"DataBeginningIndex" => $sql["id"],
		"DataLength" => count($sql)
	]);
}
// ^TODO: find location ids
?>