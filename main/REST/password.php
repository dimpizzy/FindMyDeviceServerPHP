<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "hashedPassword", "salt", "privkey"])) {
	$username = checkToken($_PUT["IDT"]);

	sqlquery("UPDATE user(username, password, salt, pri) VALUES (:user, :pw, :salt, :pri);", [
		":user" => $username,
		":pw" => $_PUT["hashedPassword"],
		":salt" => $_PUT["salt"],
		":pri" => $_PUT["privkey"]
	]);

	response([
		"state" => "ok",
		"IDT" => $_PUT["IDT"],
		"Data" => true
	]);
}
?>