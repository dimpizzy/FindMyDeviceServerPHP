<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_POST, ["IDT", "Data"])) {
	$username = checkToken($_POST["IDT"]);

	sqlquery(implode("\n", [
		"DELETE FROM token WHERE username = :user;",
		"DELETE FROM location WHERE username = :user;",
		"DELETE FROM picture WHERE username = :user;",
		"DELETE FROM command WHERE username = :user;",
		"DELETE FROM user WHERE username = :user;"
	]), [
		":user" => $username
	]);

	response([
		"state" => "ok"
	]);
}

if(exists($_PUT, ["hashedPassword", "pubkey", "privkey"])) {
	$dID = genToken($ULENGTH);

	if(!exists($_PUT, "salt")) {
		$_PUT["salt"] = "cafe";
	}

	sqlquery("INSERT INTO user(username, salt, password, pub, pri) VALUES (:user, :salt, :pw, :pub, :pri);", [
		":user" => $dID,
		":salt" => $_PUT["salt"],
		":pw" => $_PUT["hashedPassword"],
		":pub" => $_PUT["pubkey"],
		":pri" => $_PUT["privkey"]
	]);

	response([
		"DeviceId" => $dID,
		"AccessToken" => ""
	]);
}
?>