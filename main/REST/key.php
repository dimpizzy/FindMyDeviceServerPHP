<?php
include_once(dirname(__DIR__)."/main.php");
if(exists($_PUT, ["IDT", "Data"], true)) {
	$username = checkToken($_PUT["IDT"]);

	$sql = sqlquery("SELECT pri FROM user WHERE username = :user;", [
		":user" => $username
	])[0];
	$key = $sql["pri"];
	// $key = base64_encode($key);

	header("Content-Type: application/text");
	exit($key);
}
?>