let gallery;
window.onload = function() {
	initMap(document.getElementById("map"));
	FMD.version();

	gallery = new Gallery({
		elem: document.getElementById("gallery")
	});

	let d = document.getElementById("drange");
	d.onload =
	d.onchange = function(e) {
		if(e.drange) {
			MAP.filter({
				from: new Date(e.drange.left * 1000),
				to: new Date(e.drange.right * 1000),
			});

			document.getElementById("from").innerText = new Date(e.drange.left * 1000).toLocaleString();
			document.getElementById("to").innerText = new Date(e.drange.right * 1000).toLocaleString();
		}
	}
}

function initMap(map) {
	MAP.map = L.map(map, {
		center: [0, 0],
		zoom: 1.5
	});
	MAP.map.zoomControl.setPosition("bottomleft");

	L.tileLayer("https://{s}.tile.osm.org/{z}/{x}/{y}.png", {
		attribution: "&copy; <a href=\"https://osm.org/copyright\">OpenStreetMap</a> contributors"
	}).addTo(MAP.map);

	// MAP.setMaxZoom(20);
	// ^ not usable
}

function request(url, body = "", method = "PUT") {
	return new Promise(function(resolve, reject) {	
		if(typeof body == "object") {
			body = JSON.stringify(body);
		}

		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if(this.status == 200 && this.readyState == 4) {
				let that = this;
				resolve({
					text: function() {
						return that.responseText;
					},
					json: function() {
						try {
							return JSON.parse(that.responseText);
						} catch(error) {
							return this.text();
						}
					}
				});
			}
		}
		xhr.open(method, url);
		if(method !== "GET") {
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(body);
		} else {
			xhr.send();
		}
	});
}



async function getlocs(event) {
	event.target.disabled = true;

	let locs = await FMD.locate();

	let pre = null;
	for(let loc of locs) {
		let div = document.createElement("div");
		let date = document.createElement("div");
		date.innerText = "Date: "+loc.date.toLocaleString()
		div.append(date);

		let prov = document.createElement("div");
		prov.innerText = "Provider: "+loc.provider;
		div.append(prov);

		let bat = document.createElement("div");
		bat.innerText = "Battery: "+loc.bat;
		div.append(bat);

		loc.marker = L.marker([loc.lat, loc.lon])
		.addTo(MAP.map)
		.bindPopup(div.outerHTML);

		if(pre !== null) {
			pre.line = L.polyline([
				[pre.lat, pre.lon],
				[loc.lat, loc.lon]
			]).addTo(MAP.map);

			MAP.locations.push(pre);
		}
		pre = loc;
	}

	let d = document.getElementById("drange");
	d.drange = new DRange(d, {
		min: pre.date / 1000,
		max: locs[0].date / 1000,
		step: 60,
		left: locs[2].date / 1000,
		right: "max"
	});

	event.target.disabled = false;
	document.getElementsByTagName("nav")[0].classList.remove("hidden");
}

async function takePicture(side = "front") {
	await FMD.sendCommand("camera " + side);
	setTimeout(async function() {
		let pic = await FMD.getPicture()[0];
		gallery.addPicture(pic);
	}, 1000);
}

async function showPictures(event) {
	event.target.disabled = true;
	let pics = await FMD.getPicture("all");
	gallery.initGallery(pics);

	event.target.disabled = false;
	document.getElementById("overlay").classList.remove("hidden");
}