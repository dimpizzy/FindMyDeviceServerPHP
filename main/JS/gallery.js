class Gallery {
	constructor({elem = document.body}) {
		if(typeof elem == "string")
			elem = document.querySelector(elem);
		if(elem == null || elem == undefined) {
			console.error("DRange: No valid element or query");
			return;
		}
		this.elem = elem;
		this.elem.innerHTML = "";
		this.elem.classList.add("gallery")

		this.initGallery();
	}

	initGallery(pictures = []) {
		for(let pic of pictures) {
			this.addPicture(pic);
		}
	}

	addPicture(picture) {
		let index = this.elem.children.length;

		let label = document.createElement("label");
		label.id = "img" + index;
		label.className = "img";
		this.elem.append(label);

		let input = document.createElement("input");
		input.type = "checkbox";
		input.onclick = function() {
			let cL = document.body.classList;
			if(this.checked) {
				cL.add("noscroll");
				return;
			}
			cL.remove("noscroll");
		}
		input.hidden = true;
		label.append(input);

		let img = document.createElement("img");
		img.src = picture;
		img.alt = "img" + index;
		label.append(img);
	}
}