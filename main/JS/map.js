let MAP = new (class {
	constructor() {
		this.map = null;
		this.date = {
			from: new Date(),
			to: new Date()
		};
		this.locations = [];
	}

	filter({from = new Date(), to = new Date()}) {
		this.date = {
			from: from,
			to: to
		};

		let pre;
		for(let loc of this.locations) {
			let m = loc.marker.getElement();
			m.style.visibility = "hidden";
			if(loc.line) loc.line.getElement().style.visibility = "hidden";

			if(this.date.from < loc.date && loc.date < this.date.to) {
				m.style.visibility = "visible";
				if(pre && pre.line) pre.line.getElement().style.visibility = "visible";
			}
			pre = loc;
		}
	}
})();