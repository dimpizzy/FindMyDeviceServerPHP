let FMD = new (class {
	#id;
	#pw = {
		plain: "",
		hash: ""
	};
	#key = null;
	#loc = {
		first: 0,
		i: 0,
		last: 0
	};

	async version(id = "version") {
		let version = document.getElementById(id);
		version.innerText = await request("/version", null, "GET").then(res => res.text());
	}

	async locate(index = -1) {
		if(this.getID() == false || this.getPW() == false) {
			alert("You need to enter a device-ID and a password");
			return false;
		}

		let token = await this.requestAccess();
		if(token == false) {
			alert("No Access allowed");
			return false;
		}

		let dataSize = await this.locationData(token, index);
		if(dataSize == false) {
			alert("No Data found");
			return false;
		}

		let locs = await request("/location", {
			IDT: token,
			Data: this.#loc.i
		}).then(res => res.json());

		let key = await this.getKey(token);
		if(key == false) {
			alert("No key found");
			return false;
		}

		let crypt = new JSEncrypt();
		crypt.setPrivateKey(this.#key);
		locs = locs.location;
		for(let loc in locs) {
			for(let attr in locs[loc]) {
				if(attr == "date") {
					locs[loc][attr] = new Date(locs[loc][attr]);
					continue;
				}
				locs[loc][attr] = crypt.decrypt(locs[loc][attr]);
			}
		}

		this.checkNewLocations();

		return locs;
	}

	getID(id = "did") {
		let did = document.getElementById(id).value;
		if(did !== "") {
			this.#id = did;
			return true;
		}
		return false;
	}

	getPW(id = "passwd") {
		let pw = document.getElementById(id).value;
		if(pw !== "") {
			this.#pw.plain = pw;
			this.#pw.hash = CryptoJS.PBKDF2(this.#pw.plain, CryptoJS.enc.Hex.parse("cafe"), {
				keySize: 8,
				iterations: 1867 * 2
			}).toString().toUpperCase();
			return true;
		}
		return false;
	}

	async requestAccess() {
		let res = await request("/requestAccess", {
			IDT: this.#id,
			Data: this.#pw.hash
		}).then(res => res.json());
		if(!res.Data) {
			if(res.message) alert(res.message);
			return false;
		}
		return res.Data;
	}

	async locationData(token, index = -1) {
		let ld = await request("/locationDataSize", {
			IDT: token,
			Data: index
		}).then(res => res.json());
		if(ld !== "") {
			if(index !== -2) {
				this.#loc.last = ld.DataLength;
				this.#loc.first = ld.DataBeginningIndex;
				this.#loc.i = index;
				this.#loc.i = Math.max(0, this.#loc.i);
				this.#loc.i = Math.min(this.#loc.i, this.#loc.last);
			}
			return ld;
		}
		return false;
	}

	async getKey(token) {
		let key = await request("/key", {
			IDT: token,
			Data: this.#loc.i
		}).then(res => res.text());
		if(key !== "") {
			this.#key = this.decryptAES(key);

			return true;
		}
		return false;
	}

	decryptAES(c, pass = this.#pw.plain) {
		let keySize = 256;
		let ivSize = 128;
		let iterationCount = 1867;

		let ivLength = ivSize / 4;
		let saltLength = keySize / 4;

		let iv = c.substr(saltLength, ivLength);
		let enc = c.substring(ivLength + saltLength);

		let salt = c.substr(0, saltLength);
		let key = CryptoJS.PBKDF2(pass, CryptoJS.enc.Hex.parse(salt), {
			keySize: keySize / 32,
			iterations: iterationCount
		});

		let cP = CryptoJS.lib.CipherParams.create({
			ciphertext: CryptoJS.enc.Base64.parse(enc)
		});
		let dec = CryptoJS.AES.decrypt(cP, key, { iv: CryptoJS.enc.Hex.parse(iv) });
		try {
			return dec.toString(CryptoJS.enc.Utf8);
		} catch (error) {
			return null;
		}
	}

	checkNewLocations() {
		if(this.loop) clearInterval(this.loop);
		let that = this;
		this.loopf = function() {
			let token = that.requestAccess();
			if(token == false) {
				alert("No Access allowed");
				return false;
			}

			let dataSize = that.locationData(token, -2);
			if(dataSize == false) {
				alert("No Data found");
				return false;
			}

			if(that.#loc.last < dataSize.DataLength) {
				that.#loc.last = dataSize.DataLength;
				that.#loc.first = dataSize.DataBeginningIndex;
				// alert("New Data");
			}
		}
		this.loop = setInterval(this.loopf, 180000);
	}

	async sendCommand(cmd) {
		if(this.getID() == false || this.getPW() == false) {
			alert("You need to enter a device-ID and a password");
			return false;
		}

		let token = await this.requestAccess();
		if(token == false) {
			alert("No Access allowed");
			return false;
		}

		let poss = [
			"locate",
			"ring",
			"lock",
			"delete",
			"camera front",
			"camera back"
		];
		if(poss.indexOf(cmd) < 0) return;

		let a = await request("/command", {
			IDT: token,
			Data: cmd
		}, "POST");
		// alert("Command send");	
	}

	async getPicture(index = -1) {
		let token = await this.requestAccess();
		if(token == false) {
			alert("No Access allowed");
			return false;
		}

		let res = await request("/picture", {
			IDT: token,
			Data: index
		}).then(res => res.json());
		let pics = [];
		for(let pic of res.pictures) {
			pic = pic.split("___PICTURE-DATA___");

			let crypt = new JSEncrypt();
			crypt.setPrivateKey(this.#key);
			pic[0] = crypt.decrypt(pic[0]);
			pic = this.decryptAES(pic[1], pic[0]);

			pics.push("data:image/jpeg;base64," + pic);
		}

		return pics;
	}
})();