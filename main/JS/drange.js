class DRange {
	constructor(elem, conf = {min: 0, max: 100, step: 1, left: 0, right: 100}) {
		if(typeof elem == "string")
			elem = document.querySelector(elem);
		if(elem == null || elem == undefined) {
			console.error("DRange: No valid element or query");
			return;
		}
		this.elem = elem;
		this.elem.innerHTML = "";
		this.elem.classList.add("drange");

		this.initDoubleRange();

		this.conf = {
			min: 0,
			max: 100,
			step: 1,
			left: 0,
			right: 100
		};
		this.updateConf(conf, true);
		this.onload({});
	}

	initDoubleRange() {
		function createInput(that) {
			let range = document.createElement("input");
			range.type = "range";
			range.oninput = function(e) {
				that.oninput(this, e);
			}
			range.onchange = function(e) {
				that.onchange(this, e);
			}
			return range;
		}

		this.slider = document.createElement("div");
		this.slider.className = "slider";
		this.elem.append(this.slider);

		this.lrange = createInput(this);
		this.lrange.className = "lrange";
		this.slider.append(this.lrange);

		this.lthumb = document.createElement("div");
		this.lthumb.className = "lthumb";
		this.slider.append(this.lthumb);

		this.range = document.createElement("div");
		this.range.className = "range";
		this.slider.append(this.range);

		this.rrange = createInput(this);
		this.rrange.className = "rrange";
		this.slider.append(this.rrange);

		this.rthumb = document.createElement("div");
		this.rthumb.className = "rthumb";
		this.slider.append(this.rthumb);


		// let info = document.createElement("div");
		// info.className = "info";
		// this.elem.append(info);

		// this.from = document.createElement("span");
		// this.from.className = "from";
		// info.append(this.from);

		// this.to = document.createElement("span");
		// this.to.className = "to";
		// info.append(this.to);

		this.updateConf();
	}

	updateConf(conf = {min: 0, max: 100, step: 1, left: 0, right: 100}, init = false) {
		conf = {
			...this.conf,
			...conf
		};
		for(let attr in this.conf) {
			if(!conf[attr] || this.conf[attr] !== conf[attr] || init) {
				this.conf[attr] = conf[attr];
				if(typeof conf[attr] == "string") {
					this.conf[attr] = conf[conf[attr]];
				}
	
				switch(attr) {
					case "min":
					case "max":
					case "step":
						this.lrange[attr] =
						this.rrange[attr] =
						this.conf[attr];
						break;
					case "left":
						this.lrange.value = this.conf.left;
						this.lthumb.style.left = this.toPercent(this.conf.left) + "%";
						
						this.range.style.left = this.toPercent(this.conf.left) + "%";
						break;
					case "right":
						this.rrange.value = this.conf.right;
						this.rthumb.style.left = this.toPercent(this.conf.right) + "%";

						this.range.style.right = (100 - this.toPercent(this.conf.right)) + "%";
						break;
				}
			}
		}
	}

	toPercent(value) {
		if(typeof value !== "number") value = parseInt(value);
		let range = this.conf.max - this.conf.min;
		value = (value - this.conf.min) * 100;
		return value / range;
	}

	onload(e) {
		if(typeof this.elem.onload == "function") {
			e.drange = {
				left: parseInt(this.lrange.value),
				right: parseInt(this.rrange.value)
			}
			this.elem.onload(e);
		}
	}

	oninput(elem, e) {
		let conf = elem == this.lrange ? {
			left: Math.min(this.lrange.value, this.rrange.value - 1)
		} : {
			right: Math.max(this.rrange.value, this.lrange.value - (-1))
		};
		this.updateConf(conf);

		if(typeof this.elem.oninput == "function") {
			e.drange = {
				left: parseInt(this.lrange.value),
				right: parseInt(this.rrange.value)
			}
			this.elem.oninput(e);
		}
	}

	onchange(elem, e) {
		let conf = elem == this.lrange ? {
			left: Math.min(this.lrange.value, this.rrange.value - 1)
		} : {
			right: Math.max(this.rrange.value, this.lrange.value - (-1))
		};
		this.updateConf(conf);

		if(typeof this.elem.onchange == "function") {
			e.drange = {
				left: parseInt(this.lrange.value),
				right: parseInt(this.rrange.value)
			}
			this.elem.onchange(e);
		}
	}
}