# FindMyDeviceServerPHP (FMDSP)

This is a port of the original [FindMyDeviceServer](https://gitlab.com/Nulide/findmydeviceserver) from Nulide

It is written in PHP and has its own frontend

# How to install and deploy

1. download the [latest release](../../../-/releases)
2. configure your **conf.php** file in the `/main` folder
    1. change the `$ROOT` to the right folder
    2. change the DB credentials (DBNAME, DBUSERNAME, DBPASSWORD)
    3. change the `$TLENGTH` and/or `$ULENGTH` (optional)
3. execute the `install.php` and wait till 

# What if you update your FMDSP

1. just execute the `update.php` file in the `/main` folder

# If you have an issue

Please look first into the [wiki](../../../-/wikis) and if it doesn't help make a new issue.

# Personal update

I have a medical issue and need time to recover and solve this issue. If some features don't work, I'm sorry but I will take some time without many updates.

Maybe help me, by making contributions and MRs. I will make a thank you page for all contributers.
